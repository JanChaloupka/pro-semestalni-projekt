db = db.getSiblingDB('kappadb');

// 	- aspon 5 filmů
// 	- aspon 4 promítání na každý den pro celkem 5 dní od čtvrtka
// 	- v každém promítání mít aspon 1 rezervaci se 3 sedadly

db.createCollection('movies');
const moviesInsert = db.movies.insertMany([
    {
        name: 'Není čas zemřít',
        year: 2021,
        director: 'Cary Joji Fukunaga',
        actors: ' Daniel Craig, Léa Seydoux, Rami Malek, Lashana Lynch, Ralph Fiennes, Ben Whishaw, Ana de Armas, Naomie Harris, David Dencik, Rory Kinnear',
        description: 'James Bond, agent 007, skončil aktivní službu a užívá si zasloužený odpočinek na Jamajce. Klid však nemá dlouhého trvání – objeví se totiž jeho starý přítel Felix Leiter ze CIA a požádá ho o pomoc. Mise na záchranu uneseného vědce se ukáže být zrádnější, než se na začátku zdálo. Bond se během vyšetřování dostane na stopu nevyzpytatelného zločince, který disponuje nebezpečnou a velmi ničivou novou technologií.',
        length: 163,
        image: 'http://image.tmdb.org/t/p/w500//iUgygt3fscRoKWCV1d0C7FbM9TP.jpg'
    },
    {
        name: 'Poslední samuraj',
        year: 2003,
        director: 'Edward Zwick',
        actors: 'Ken Watanabe, Tom Cruise, William Atherton, Timothy Spall, Chad Lindberg, Billy Connolly, Tony Goldwyn, Masato Harada, Kojuki Kató, Šun Sugata',
        description: 'Nathan Algren se stal hrdinou občanské války a tažení proti indiánům. V roce 1876 je z něj však troska a alkoholik. Poručík Gant ho přivede na setkání s japonským průmyslníkem Omurou, který mu nabídne práci při výcviku modernizované japonské armády. V Japonsku čeká Nathana tlumočník Simon Graham, který mu vysvětlí, že mladý císař Meidži chce otevřít zemi západní civilizaci, proti sobě však má rebely z řad samurajů, kteří brání dávné japonské tradice. Když rebelové, vedení samurajem Kacumotem, napadnou železniční stanici, dostane jednotka příkaz k nasazení do boje.',
        length: 148,
        image: 'http://image.tmdb.org/t/p/w500//lsasOSgYI85EHygtT5SvcxtZVYT.jpg'
    },
    {
        name: 'Simpsonovi ve filmu',
        year: 2007,
        director: 'David Silverman',
        actors: 'Dan Castellaneta, Julie Kavner, Nancy Cartwright, Yeardley Smith, Hank Azaria, Harry Shearer, Albert Brooks, Billie Joe Armstrong, Tress MacNeille',
        description: 'Jeden z nejúspěšnějších televizních seriálů se dočkal i celovečerního filmového zpracování. Populární rodinka Simpsonových ze Springfieldu tentokrát čelí globálním výzvám a Homer provede nejhorší věc svého života. Homer totiž do jezera vypustí jímku s exkrementy svého oblíbeného pašíka. Pro springfieldské jezero, které už takhle slouží jako oblíbená skládka občanů a dosahuje maximálního stupně znečištění, je to poslední kapka. A je to i poslední kapka v poháru trpelivosti prezidenta. Ten se proto rozhodne nepohodlné město zcela izolovat pod speciálním skleněným poklopem a oficiálně ho vymazat ze všech map. Jenže s tím se Homer smířit nehodlá.',
        length: 83,
        image: 'http://image.tmdb.org/t/p/w500//gzb6P78zeFTnv9eoFYnaJ2YrZ5q.jpg'
    },
    {
        name: 'Duna',
        year: 2021,
        director: 'Denis Villeneuve',
        actors: 'Timothée Chalamet, Rebecca Ferguson, Oscar Isaac, Jason Momoa, Josh Brolin, Javier Bardem, Stellan Skarsgård, Zendaya, Dave Bautista',
        description: 'Kultovní sci-fi dílo vypráví o mocenských bojích uvnitř galaktického Impéria, v nichž jde o ovládnutí planety Arrakis: zdroje vzácného koření - melanže, jež poskytuje zvláštní psychické schopnosti, které umožňují cestování vesmírem. Padišáh imperátor svěří správu nad Arrakisem a s ní i komplikovanou těžbu neobyčejné látky vévodovi rodu Atreidů. Celá anabáze je ale součástí spiknutí, z něhož se podaří vyváznout jen vévodovu synovi Paulovi a jeho matce, kteří uprchnou do pouště. Jejich jedinou nadějí jsou nedůvěřiví domorodí obyvatelé fremeni, schopní trvale přežít ve vyprahlé pustině. Mohl by Paul Atreides být spasitelem, na kterého tak dlouho čekají?',
        length: 155,
        image: 'http://image.tmdb.org/t/p/w500//d5NXSklXo0qyIYkgV94XAgMIckC.jpg'
    },
    {
        name: 'Skyfall',
        year: 2012,
        director: 'Sam Mendes',
        actors: ' Daniel Craig, Judi Dench, Ralph Fiennes, Javier Bardem, Naomie Harris, Bérénice Marlohe, Ben Whishaw, Helen McCrory, Albert Finney, Rory Kinnear',
        description: 'Britská tajná služba MI6 se ocitá v nelehké situaci, ze základny v Turecku byl ukraden harddisk se seznamem agentů. Agent 007 James Bond a jeho kolegyně Eve se pokusili lupiče zadržet, Bond byl však během akce postřelen a zmizel neznámo kam. Šéfce MI6, M, nezbývá nic jiného než prohlásit Bonda za mrtvého. Nový předseda bezpečnostně-zpravodajského výboru Mallory je situací silně znepokojen; nejedná se totiž o první případ krádeže seznamu tajných agentů.',
        length: 143,
        image: 'http://image.tmdb.org/t/p/w500//iANstMt9B4unvLMLLOWnBnZIt9b.jpg'
    }
]);
const moviesIds = moviesInsert.insertedIds;

db.createCollection('cinemaHall');
const cinemaHallsInsert = db.cinemaHall.insertMany([
    {
        name: "Sál 1",
        support3D: false,
        betterSound: true,
        seats: [
            ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"],
            ["11", "12", "13", "14", "15", "16", "17", "18", "19", "20"],
            ["21", "22", "23", "24", "25", "26", "27", "28", "29", "30"],
            ["31", "32", "33", "34", "35", "36", "37", "38", "39", "40"],
            ["41", "42", "43", "44", "45", "46", "47", "48", "49", "50"],
            ["51", "52", "53", "54", "55", "56", "57", "58", "59", "60"],
        ]
    },
    {
        name: "Sál 2",
        support3D: false,
        betterSound: true,
        seats: [
            ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"],
            ["11", "12", "13", "14", "15", "16", "17", "18", "19", "20"],
            ["21", "22", "23", "24", "25", "26", "27", "28", "29", "30"],
            ["31", "32", "33", "34", "35", "36", "37", "38", "39", "40"],
            ["41", "42", "43", "44", "45", "46", "47", "48", "49", "50"],
            ["51", "52", "53", "54", "55", "56", "57", "58", "59", "60"],
        ]
    },
    {
        name: "Sál 3",
        support3D: true,
        betterSound: true,
        seats: [
            ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15"],
            ["16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30"],
            ["31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45"],
            ["46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59", "60"],
            ["61", "62", "63", "64", "65", "66", "67", "68", "69", "70", "71", "72", "73", "74", "75"],
            ["76", "77", "78", "79", "80", "81", "82", "83", "84", "85", "86", "87", "88", "89", "90"],
            ["91", "92", "93", "94", "95", "96", "97", "98", "99", "100", "101", "102", "103", "104", "105"],
            ["106", "107", "108", "109", "110", "111", "112", "113", "114", "115", "116", "117", "118", "119", "120"],
            ["121", "122", "123", "124", "125", "126", "127", "128", "129", "130", "131", "132", "133", "134", "135"]
        ]
    }
]);
const cinemaHallsIds = cinemaHallsInsert.insertedIds;

db.createCollection('screening');
const screeningsInsert = db.screening.insertMany([
    // Čtvrtek
    {
        betterSound: true,
        support3D: false,
        ticketPrice: 250,
        datetime: new Date('2022-01-20T17:00:00.000Z'),
        movie: {$ref: 'movies', $id: moviesIds[0]}, // Není čas zemřít
        cinemaHall: {$ref: 'cinemaHall', $id: cinemaHallsIds[0]}
    },
    {
        betterSound: true,
        support3D: false,
        ticketPrice: 250,
        datetime: new Date('2022-01-20T20:00:00.000Z'),
        movie: {$ref: 'movies', $id: moviesIds[0]}, // Není čas zemřít
        cinemaHall: {$ref: 'cinemaHall', $id: cinemaHallsIds[0]}
    },
    {
        betterSound: false,
        support3D: false,
        ticketPrice: 200,
        datetime: new Date('2022-01-20T14:00:00.000Z'),
        movie: {$ref: 'movies', $id: moviesIds[1]}, // Poslední samuraj
        cinemaHall: {$ref: 'cinemaHall', $id: cinemaHallsIds[0]}
    },
    {
        betterSound: false,
        support3D: false,
        ticketPrice: 200,
        datetime: new Date('2022-01-20T17:00:00.000Z'),
        movie: {$ref: 'movies', $id: moviesIds[1]}, // Poslední samuraj
        cinemaHall: {$ref: 'cinemaHall', $id: cinemaHallsIds[1]}
    },
    {
        betterSound: false,
        support3D: false,
        ticketPrice: 150,
        datetime: new Date('2022-01-20T16:00:00.000Z'),
        movie: {$ref: 'movies', $id: moviesIds[2]}, // Simpsonovi
        cinemaHall: {$ref: 'cinemaHall', $id: cinemaHallsIds[2]}
    },
    {
        betterSound: true,
        support3D: true,
        ticketPrice: 250,
        datetime: new Date('2022-01-20T20:00:00.000Z'),
        movie: {$ref: 'movies', $id: moviesIds[3]}, // Duna
        cinemaHall: {$ref: 'cinemaHall', $id: cinemaHallsIds[2]}
    },
    // Pátek
    {
        betterSound: true,
        support3D: false,
        ticketPrice: 250,
        datetime: new Date('2022-01-21T17:00:00.000Z'),
        movie: {$ref: 'movies', $id: moviesIds[0]}, // Není čas zemřít
        cinemaHall: {$ref: 'cinemaHall', $id: cinemaHallsIds[0]}
    },
    {
        betterSound: true,
        support3D: false,
        ticketPrice: 250,
        datetime: new Date('2022-01-21T20:00:00.000Z'),
        movie: {$ref: 'movies', $id: moviesIds[0]}, // Není čas zemřít
        cinemaHall: {$ref: 'cinemaHall', $id: cinemaHallsIds[1]}
    },
    {
        betterSound: false,
        support3D: false,
        ticketPrice: 200,
        datetime: new Date('2022-01-21T15:00:00.000Z'),
        movie: {$ref: 'movies', $id: moviesIds[1]}, // Poslední samuraj
        cinemaHall: {$ref: 'cinemaHall', $id: cinemaHallsIds[0]}
    },
    {
        betterSound: false,
        support3D: false,
        ticketPrice: 200,
        datetime: new Date('2022-01-21T17:00:00.000Z'),
        movie: {$ref: 'movies', $id: moviesIds[1]}, // Poslední samuraj
        cinemaHall: {$ref: 'cinemaHall', $id: cinemaHallsIds[1]}
    },
    {
        betterSound: true,
        support3D: false,
        ticketPrice: 200,
        datetime: new Date('2022-01-21T20:00:00.000Z'),
        movie: {$ref: 'movies', $id: moviesIds[4]}, // Skyfall
        cinemaHall: {$ref: 'cinemaHall', $id: cinemaHallsIds[2]}
    },
    {
        betterSound: true,
        support3D: true,
        ticketPrice: 250,
        datetime: new Date('2022-01-21T17:00:00.000Z'),
        movie: {$ref: 'movies', $id: moviesIds[3]}, // Duna
        cinemaHall: {$ref: 'cinemaHall', $id: cinemaHallsIds[2]}
    },
    // Sobota
    {
        betterSound: false,
        support3D: false,
        ticketPrice: 150,
        datetime: new Date('2022-01-22T14:00:00.000Z'),
        movie: {$ref: 'movies', $id: moviesIds[2]}, // Simpsonovi
        cinemaHall: {$ref: 'cinemaHall', $id: cinemaHallsIds[2]}
    },
    {
        betterSound: true,
        support3D: false,
        ticketPrice: 250,
        datetime: new Date('2022-01-22T17:00:00.000Z'),
        movie: {$ref: 'movies', $id: moviesIds[0]}, // Není čas zemřít
        cinemaHall: {$ref: 'cinemaHall', $id: cinemaHallsIds[0]}
    },
    {
        betterSound: true,
        support3D: false,
        ticketPrice: 250,
        datetime: new Date('2022-01-22T20:00:00.000Z'),
        movie: {$ref: 'movies', $id: moviesIds[0]}, // Není čas zemřít
        cinemaHall: {$ref: 'cinemaHall', $id: cinemaHallsIds[1]}
    },
    {
        betterSound: true,
        support3D: false,
        ticketPrice: 250,
        datetime: new Date('2022-01-22T15:00:00.000Z'),
        movie: {$ref: 'movies', $id: moviesIds[4]}, // Skyfall
        cinemaHall: {$ref: 'cinemaHall', $id: cinemaHallsIds[1]}
    },
    {
        betterSound: true,
        support3D: false,
        ticketPrice: 250,
        datetime: new Date('2022-01-22T20:00:00.000Z'),
        movie: {$ref: 'movies', $id: moviesIds[4]}, // Skyfall
        cinemaHall: {$ref: 'cinemaHall', $id: cinemaHallsIds[0]}
    },
    {
        betterSound: true,
        support3D: true,
        ticketPrice: 250,
        datetime: new Date('2022-01-22T17:00:00.000Z'),
        movie: {$ref: 'movies', $id: moviesIds[3]}, // Duna
        cinemaHall: {$ref: 'cinemaHall', $id: cinemaHallsIds[2]}
    },
    {
        betterSound: true,
        support3D: true,
        ticketPrice: 250,
        datetime: new Date('2022-01-22T20:30:00.000Z'),
        movie: {$ref: 'movies', $id: moviesIds[3]}, // Duna
        cinemaHall: {$ref: 'cinemaHall', $id: cinemaHallsIds[2]}
    },
    // Neděle
    {
        betterSound: false,
        support3D: false,
        ticketPrice: 150,
        datetime: new Date('2022-01-23T14:00:00.000Z'),
        movie: {$ref: 'movies', $id: moviesIds[2]}, // Simpsonovy
        cinemaHall: {$ref: 'cinemaHall', $id: cinemaHallsIds[2]}
    },
    {
        betterSound: false,
        support3D: false,
        ticketPrice: 150,
        datetime: new Date('2022-01-23T16:00:00.000Z'),
        movie: {$ref: 'movies', $id: moviesIds[2]}, // Simpsonovy
        cinemaHall: {$ref: 'cinemaHall', $id: cinemaHallsIds[2]}
    },
    {
        betterSound: true,
        support3D: false,
        ticketPrice: 250,
        datetime: new Date('2022-01-23T20:00:00.000Z'),
        movie: {$ref: 'movies', $id: moviesIds[3]}, // Duna
        cinemaHall: {$ref: 'cinemaHall', $id: cinemaHallsIds[1]}
    },
    {
        betterSound: true,
        support3D: false,
        ticketPrice: 250,
        datetime: new Date('2022-01-23T17:00:00.000Z'),
        movie: {$ref: 'movies', $id: moviesIds[3]}, // Duna
        cinemaHall: {$ref: 'cinemaHall', $id: cinemaHallsIds[0]}
    },
    {
        betterSound: false,
        support3D: false,
        ticketPrice: 150,
        datetime: new Date('2022-01-23T20:00:00.000Z'),
        movie: {$ref: 'movies', $id: moviesIds[1]}, // Samuraj
        cinemaHall: {$ref: 'cinemaHall', $id: cinemaHallsIds[0]}
    },
    {
        betterSound: false,
        support3D: false,
        ticketPrice: 150,
        datetime: new Date('2022-01-23T17:00:00.000Z'),
        movie: {$ref: 'movies', $id: moviesIds[1]}, // Samuraj
        cinemaHall: {$ref: 'cinemaHall', $id: cinemaHallsIds[1]}
    },
])
const screeningIds = screeningsInsert.insertedIds;

const reservations = [];
screeningIds.forEach(screeningId => {
    reservations.push({
        datetime: new Date(),
        email: 'krejcda2@uhk.cz',
        personName: 'Daniel Krejčí',
        screening: {$ref: 'screening', $id: screeningId},
        seatsNumbers: getSeats()
    })
})

db.createCollection('reservation')
db.reservation.insertMany(reservations)

function getSeats() {
    const seats = [];
    const numberOfSeats = Math.floor(Math.random() * 20) + 1;
    for (let i = 0; i < numberOfSeats; i++) {
        seats.push((Math.floor(Math.random() * 60) + 1).toString())
    }
    return seats;
}