package cz.uhk.pro.kappastar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KappastarApplication {

	public static void main(String[] args) {
		SpringApplication.run(KappastarApplication.class, args);
	}

}
