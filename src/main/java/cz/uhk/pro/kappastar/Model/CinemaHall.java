package cz.uhk.pro.kappastar.Model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

import java.util.Arrays;
import java.util.concurrent.atomic.AtomicInteger;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CinemaHall {
    @Id
    String id;
    String name;
    boolean support3D;
    boolean betterSound;
    String[][] seats = {};

    public int getSeatsCount() {
        AtomicInteger count = new AtomicInteger();
        Arrays.stream(this.seats).forEach(row -> Arrays.stream(row).forEach(column -> count.getAndIncrement()));
        return count.get();
    }

    public CinemaHall(String name, boolean support3D, boolean betterSound, String[][] seats) {
        this.name = name;
        this.support3D = support3D;
        this.betterSound = betterSound;
        this.seats = seats;
    }

    @Override
    public String toString() {
        return "CinemaHall{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", support3D=" + support3D +
                ", betterSound=" + betterSound +
                ", seats=" + (seats != null ? Arrays.deepToString(seats) : "[null]") +
                '}';
    }
}

