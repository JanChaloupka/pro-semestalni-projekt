package cz.uhk.pro.kappastar.Model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Document(collection = "movies")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Movie {
    @Id
    String id;

    @NotNull
    @NotEmpty
    String name;

    @NotNull
    @NotEmpty
    int year;

    @NotNull
    @NotEmpty
    String director;

    @NotNull
    @NotEmpty
    String actors;

    @NotNull
    @NotEmpty
    String description;

    @NotNull
    @NotEmpty
    int length;

    String image;

    public Movie(String name, int year, String director, String actors, String description, int length, String image) {
        this.name = name;
        this.year = year;
        this.director = director;
        this.actors = actors;
        this.description = description;
        this.length = length;
        this.image = image;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", year='" + year + '\'' +
                ", director=" + director + '\'' +
                ", actors=" + actors + '\'' +
                ", description=" + description + '\'' +
                ", length=" + length + '\'' +
                ", image=" + image +
                '}';
    }
}
