package cz.uhk.pro.kappastar.Model;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document(collection = "screening")
@Data
@AllArgsConstructor
public class Screening {

    @Id
    String id;

    @DBRef
    @NotNull
    @NotEmpty
    Movie movie;

    @DBRef
    @NotNull
    @NotEmpty
    CinemaHall cinemaHall;

    @NotNull
    @NotEmpty
    @FutureOrPresent
    Date datetime;

    @NotNull
    @NotEmpty
    @PositiveOrZero
    int ticketPrice;

    @NotNull
    boolean support3D;

    @NotNull
    boolean betterSound;

    public Screening(Movie movie, CinemaHall cinemaHall, Date datetime, int ticketPrize, boolean support3D, boolean betterSound) {
        this.movie = movie;
        this.cinemaHall = cinemaHall;
        this.datetime = datetime;
        this.support3D = support3D;
        this.ticketPrice = ticketPrize;
        this.betterSound = betterSound;
    }

    public Screening() {
        this.movie = new Movie();
        this.cinemaHall = new CinemaHall();
    }

    public Date getEndDatetime() {
        if (this.datetime != null) {
            Date endDatetime = new Date();
            endDatetime.setTime(this.datetime.getTime() + ((long) this.movie.length * 60 * 1000));
            return endDatetime;
        }
        return null;
    }

    @Override
    public String toString() {
        return "Screening{" +
                "id='" + id + '\'' +
                ", movie='" + (movie != null ? movie.toString() : "[null]") + '\'' +
                ", cinemaHall='" + (cinemaHall != null ? cinemaHall.toString() : "[null]") + '\'' +
                ", datetime=" + datetime +
                ", support3D=" + support3D +
                ", ticketPrize=" + ticketPrice +
                ", betterSound=" + betterSound +
                '}';
    }
}