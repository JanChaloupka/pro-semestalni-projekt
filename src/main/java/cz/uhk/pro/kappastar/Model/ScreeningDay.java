package cz.uhk.pro.kappastar.Model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ScreeningDay {

    @NotNull
    @NotEmpty
    String date;

    @NotNull
    @NotEmpty
    String dayName;

    @Override
    public String toString() {
        return "ScreeningDay{" +
                ", date='" + date + '\'' +
                ", dayName='" + dayName +
                '}';
    }
}