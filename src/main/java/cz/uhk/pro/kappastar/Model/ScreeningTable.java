package cz.uhk.pro.kappastar.Model;

import cz.uhk.pro.kappastar.Model.dto.ScreeningDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Arrays;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ScreeningTable {

    @NotNull
    @NotEmpty
    Movie movie;

    @NotNull
    @NotEmpty
    ArrayList<ScreeningDto>[] screenings = new ArrayList[12];

    @Override
    public String toString() {
        return "ScreeningTable{" +
                ", movie='" + (movie != null ? movie.toString() : "[null]") + '\'' +
                ", screenings='" + (screenings != null ? Arrays.toString(screenings) : "[null]") +
                '}';
    }
}