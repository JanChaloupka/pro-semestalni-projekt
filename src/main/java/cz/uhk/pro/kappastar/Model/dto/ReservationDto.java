package cz.uhk.pro.kappastar.Model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

import javax.validation.constraints.Email;
import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReservationDto {
    @Id
    String id;

    @NotNull
    @NotEmpty
    String screeningId;

    @NotNull
    @NotEmpty
    String personName;

    @NotNull
    @NotEmpty
    @Email
    String email;

    @NotNull
    @NotEmpty
    @FutureOrPresent
    Date datetime;

    @NotNull
    @NotEmpty
    List<String> seatsNumbers = new ArrayList<>();
}
