package cz.uhk.pro.kappastar.Model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

import javax.validation.constraints.*;
import java.text.SimpleDateFormat;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ScreeningDto {

    @Id
    String id;

    @NotNull
    @NotEmpty
    String movieId;

    @NotNull
    @NotEmpty
    String movieName;

    @NotNull
    @NotEmpty
    String cinemaHallId;

    @NotNull
    @NotEmpty
    String cinemaHallName;

    @NotNull
    @NotEmpty
    @FutureOrPresent
    Date datetime;

    @NotNull
    @NotEmpty
    String time;

    @NotNull
    @NotEmpty
    @PositiveOrZero
    int ticketPrice;

    @NotNull
    boolean support3D;

    @NotNull
    boolean betterSound;

    public ScreeningDto(String movieId, String movieName, String cinemaHallId, String cinemaHallName, Date datetime, String time, int ticketPrize, boolean support3D, boolean betterSound) {
        this.movieId = movieId;
        this.movieName = movieName;
        this.cinemaHallId = cinemaHallId;
        this.cinemaHallName = cinemaHallName;
        this.datetime = datetime;
        this.time = time;
        this.support3D = support3D;
        this.ticketPrice = ticketPrize;
        this.betterSound = betterSound;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setTime(Date date) {
        this.time = new SimpleDateFormat("HH:mm").format(date);
    }

    @Override
    public String toString() {
        return "Screening{" +
                "id='" + id + '\'' +
                ", movieId='" + movieId + '\'' +
                ", cinemaHallId='" + cinemaHallId + '\'' +
                ", datetime=" + datetime +
                ", time=" + time +
                ", support3D=" + support3D +
                ", ticketPrize=" + ticketPrice +
                ", betterSound=" + betterSound +
                '}';
    }
}