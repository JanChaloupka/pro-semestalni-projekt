package cz.uhk.pro.kappastar.controller;

import cz.uhk.pro.kappastar.Model.CinemaHall;
import cz.uhk.pro.kappastar.service.CinemaHallService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@Controller
@AllArgsConstructor
@RequestMapping("admin/cinema-halls")

public class AdminCinemaHallController {

    private final CinemaHallService cinemaHallService;

    //ADMIN PART
    @GetMapping
    public String getCinemaHalls(Model model) {
        model.addAttribute("cinemaHalls", cinemaHallService.getCinemaHalls());
        return "admin/cinemaHalls/list";
    }

    @GetMapping("{id}")
    public String getCinemaHallDetail(@PathVariable("id") String id, Model model) {
        Optional<CinemaHall> cinemaHall = cinemaHallService.getCinemaHall(id);

        if (cinemaHall.isPresent()) {
            model.addAttribute("cinemaHall", cinemaHall.get());
        } else {
            model.addAttribute("cinemaHall", null);
        }
        return "admin/cinemaHalls/detail";
    }

    @PostMapping("add")
    @ResponseBody
    public void addCinemaHall(@RequestBody CinemaHall cinemaHall) {
        cinemaHallService.addCinemaHall(cinemaHall);
    }

    @DeleteMapping("admin/delete/{id}")
    public void deleteCinemaHall(@PathVariable String id) {
        cinemaHallService.deleteCinemaHall(id);
    }
}
