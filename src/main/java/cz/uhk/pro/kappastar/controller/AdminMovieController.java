package cz.uhk.pro.kappastar.controller;

import cz.uhk.pro.kappastar.Model.Movie;
import cz.uhk.pro.kappastar.service.MovieService;
import cz.uhk.pro.kappastar.service.ReservationService;
import cz.uhk.pro.kappastar.service.ScreeningService;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;
@Log4j2
@Controller
@RequestMapping("admin/movies")
@AllArgsConstructor
public class AdminMovieController {

    private final MovieService movieService;

    @GetMapping
    public String getMovies(Model model) {
        model.addAttribute("movies", movieService.getAllMovies());
        return "admin/movies/list";
    }

    @GetMapping("{id}")
    public String getMovie(@PathVariable String id, Model model) {
        Optional<Movie> movie = movieService.getMovieById(id);

        if (movie.isPresent()) {
            model.addAttribute("movie", movie.get());
        } else {
            model.addAttribute("movie", null);
        }
        return "admin/movies/detail";
    }

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String getMovieDetailForm(Model model) {
        model.addAttribute("movie", new Movie());

        return "admin/movies/add";
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String createMovie(@Valid @ModelAttribute Movie movie, BindingResult bindingResult, Model model) {

        var created = movieService.addMovie(movie, bindingResult);

        if (bindingResult.hasErrors()) {
            model.addAttribute("movie", movie);
            return "admin/movies/add";
        }

        return "redirect:/admin/movies" + (created.map(screening -> "/" + screening.getId()).orElse(""));

    }

    @GetMapping("/{id}/edit")
    public String edit(@PathVariable("id") String id, Model model) {

        Optional<Movie> movieOpt = movieService.getMovieById(id);
        Movie movie = null;

        if (movieOpt.isPresent()) {
            movie = movieOpt.get();
        }

        model.addAttribute("movie", movie);

        return "admin/movies/edit";
    }

    @PostMapping("/{id}/edit")
    public String edit(@Valid @ModelAttribute Movie movie, BindingResult bindingResult, Model model) {

        var updated = movieService.updateMovie(movie, bindingResult);

        if (bindingResult.hasErrors()) {
            model.addAttribute("movie", movie);
            return "admin/movies/edit";
        }

        return "redirect:/admin/movies" + (updated.map(screening -> "/" + screening.getId()).orElse(""));
    }

    @PostMapping("/{id}/delete")
    public String delete(@PathVariable("id") String id) {
        movieService.deleteMovie(id);
        return "redirect:/admin/movies";
    }
}
