package cz.uhk.pro.kappastar.controller;

import cz.uhk.pro.kappastar.Model.dto.ScreeningDto;
import cz.uhk.pro.kappastar.Model.Screening;
import cz.uhk.pro.kappastar.service.CinemaHallService;
import cz.uhk.pro.kappastar.service.MovieService;
import cz.uhk.pro.kappastar.service.ReservationService;
import cz.uhk.pro.kappastar.service.ScreeningService;
import lombok.AllArgsConstructor;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

@Controller
@AllArgsConstructor
@RequestMapping("admin/screenings")
public class AdminScreeningController {

    private final ScreeningService screeningService;
    private final CinemaHallService cinemaHallService;
    private final MovieService movieService;
    private final ReservationService reservationService;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Date.class, new CustomDateEditor(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm"), true));
    }

    @GetMapping
    public String getAll(Model model) {
        model.addAttribute("screenings", screeningService.getScreenings());
        return "admin/screenings/list";
    }

    @GetMapping("/add")
    public String add(Model model) {

        model.addAttribute("screening", new ScreeningDto());
        model.addAttribute("cinemaHalls", cinemaHallService.getCinemaHalls());
        model.addAttribute("movies", movieService.getAllMovies());

        return "admin/screenings/add";
    }

    @PostMapping("/add")
    public String add(@Valid @ModelAttribute("screening") ScreeningDto screeningDto, BindingResult bindingResult, Model model) {
        var created = screeningService.addScreening(screeningDto, bindingResult);

        if (bindingResult.hasErrors()) {
            model.addAttribute("screening", screeningDto);
            model.addAttribute("cinemaHalls", cinemaHallService.getCinemaHalls());
            model.addAttribute("movies", movieService.getAllMovies());
            return "admin/screenings/add";
        }

        return "redirect:/admin/screenings" + (created.map(screening -> "/" + screening.getId()).orElse(""));
    }

    @GetMapping("/{id}")
    public String detail(@PathVariable("id") String id, Model model) {

        Optional<Screening> screeningOpt = screeningService.getScreeningById(id);
        Screening screening = null;

        if (screeningOpt.isPresent()) {
            screening = screeningOpt.get();
        }

        model.addAttribute("screening", screening);

        return "admin/screenings/detail";
    }

    @GetMapping("/{id}/edit")
    public String edit(@PathVariable("id") String id, Model model) {

        Optional<Screening> screening = screeningService.getScreeningById(id);
        ScreeningDto screeningDto = new ScreeningDto();

        if (screening.isPresent()) {
            screeningDto.setId(screening.get().getId());
            screeningDto.setCinemaHallId(screening.get().getCinemaHall().getId());
            screeningDto.setMovieId(screening.get().getMovie().getId());
            screeningDto.setDatetime(screening.get().getDatetime());
            screeningDto.setTicketPrice(screening.get().getTicketPrice());
            screeningDto.setBetterSound(screening.get().isBetterSound());
            screeningDto.setSupport3D(screening.get().isSupport3D());
        } else {
            screeningDto = null;
        }

        model.addAttribute("screening", screeningDto);
        model.addAttribute("cinemaHalls", cinemaHallService.getCinemaHalls());
        model.addAttribute("movies", movieService.getAllMovies());
        model.addAttribute("movieName", screening.isPresent() ? screening.get().getMovie().getName() : "");

        return "admin/screenings/edit";
    }

    @PostMapping("/{id}/edit")
    public String edit(@Valid @ModelAttribute("screening") ScreeningDto screeningDto, BindingResult bindingResult, Model model) {

        var updated = screeningService.updateScreening(screeningDto, bindingResult);

        if (bindingResult.hasErrors()) {

            Optional<Screening> screening = screeningService.getScreeningById(screeningDto.getId());

            model.addAttribute("screening", screeningDto);
            model.addAttribute("cinemaHalls", cinemaHallService.getCinemaHalls());
            model.addAttribute("movies", movieService.getAllMovies());
            model.addAttribute("movieName", screening.isPresent() ? screening.get().getMovie().getName() : "");

            return "admin/screenings/edit";
        }

        return "redirect:/admin/screenings" + (updated.map(screening -> "/" + screening.getId()).orElse(""));
    }


    @PostMapping("/{id}/delete")
    public String delete(@PathVariable("id") String id) {
        screeningService.deleteScreening(id);
        return "redirect:/admin/screenings";
    }
}
