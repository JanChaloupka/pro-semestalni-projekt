package cz.uhk.pro.kappastar.controller;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@AllArgsConstructor
@RequestMapping( "/")
public class DefaultController {

    @GetMapping
    public String index() {
        return "redirect:/screenings";
    }

    @GetMapping("/login")
    public String login() {
        return "login";
    }

    @GetMapping("/admin")
    public String admin() {
        return "redirect:/admin/screenings";
    }
}
