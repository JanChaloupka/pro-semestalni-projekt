package cz.uhk.pro.kappastar.controller;

import cz.uhk.pro.kappastar.Model.Reservation;
import cz.uhk.pro.kappastar.Model.Screening;
import cz.uhk.pro.kappastar.Model.dto.ReservationDto;
import cz.uhk.pro.kappastar.service.ReservationService;
import cz.uhk.pro.kappastar.service.ScreeningService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@Controller
@AllArgsConstructor
@RequestMapping("/reservation")
public class ReservationController {

    private final ScreeningService screeningService;
    private final ReservationService reservationService;

    @GetMapping("{id}")
    public String detail(@PathVariable String id, Model model) {

        Optional<Reservation> reservationOpt = reservationService.getReservationById(id);
        Reservation reservation = null;
        String priceSum = null;

        if (reservationOpt.isPresent()) {
            reservation = reservationOpt.get();
            priceSum = String.valueOf(reservation.getSeatsNumbers().size() * reservation.getScreening().getTicketPrice());
        }

        model.addAttribute("reservation", reservation);
        model.addAttribute("priceSum", priceSum);

        return "web/reservation/detail";
    }

    @PostMapping("/add")
    public String add(@Valid @ModelAttribute("reservation") ReservationDto reservationDto, BindingResult bindingResult, Model model) {

        var created = reservationService.addReservation(reservationDto, bindingResult);

        if (bindingResult.hasErrors()) {
            Optional<Screening> screening = screeningService.getScreeningById(reservationDto.getScreeningId());
            model.addAttribute("screening", screening.orElseGet(Screening::new));
            model.addAttribute("reservation", reservationDto);
            model.addAttribute("selectedSeats", String.join(",", reservationDto.getSeatsNumbers()));
            model.addAttribute("reservedSeats", reservationService.getReservedSeatsForScreening(reservationDto.getScreeningId()));

            return "web/reservation/add";
        }

        return "redirect:/reservation" + (created.map(reservation -> "/" + reservation.getId()).orElse(""));
    }

    @PostMapping("/{id}/delete")
    public String delete(@PathVariable("id") String id) {

        var reservation = reservationService.getReservationById(id);
        if (reservation.isEmpty()) {
            return "redirect:/screenings";
        } else {
            reservationService.deleteReservation(reservation.get().getId());
        }
        return "web/reservation/canceled";


    }
}
