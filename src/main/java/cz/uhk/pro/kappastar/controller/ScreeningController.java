package cz.uhk.pro.kappastar.controller;

import cz.uhk.pro.kappastar.Model.Screening;
import cz.uhk.pro.kappastar.Model.dto.ReservationDto;
import cz.uhk.pro.kappastar.service.ReservationService;
import cz.uhk.pro.kappastar.service.ScreeningService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

@Controller
@AllArgsConstructor
@RequestMapping("/screenings")
public class ScreeningController {

    private final ScreeningService screeningService;

    private final ReservationService reservationService;

    @GetMapping(value = {"", "{date}"})
    public String index(@PathVariable(required = false) String date, Model model) {

        if (date == null) {
            date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        }

        model.addAttribute("selectedDay", date);
        model.addAttribute("screeningDays", screeningService.getScreeningDays());
        model.addAttribute("screeningsTable", screeningService.getScreeningTable(date));
        return "web/screenings/list";
    }

    @GetMapping("/{id}/reservation")
    public String detail(@PathVariable String id, Model model) {
        Optional<Screening> screeningOpt = screeningService.getScreeningById(id);
        Screening screening = null;

        if (screeningOpt.isPresent()) {
            screening = screeningOpt.get();
        }

        model.addAttribute("screening", screening);
        model.addAttribute("reservation", new ReservationDto());
        model.addAttribute("selectedSeats", "");
        model.addAttribute("reservedSeats", reservationService.getReservedSeatsForScreening(id));


        return "web/reservation/add";
    }
}
