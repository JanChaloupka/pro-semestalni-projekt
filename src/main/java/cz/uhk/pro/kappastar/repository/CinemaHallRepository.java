package cz.uhk.pro.kappastar.repository;

import cz.uhk.pro.kappastar.Model.CinemaHall;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface CinemaHallRepository extends MongoRepository<CinemaHall, String> {
}
