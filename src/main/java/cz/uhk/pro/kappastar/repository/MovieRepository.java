package cz.uhk.pro.kappastar.repository;

import cz.uhk.pro.kappastar.Model.Movie;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface MovieRepository extends MongoRepository<Movie, String> {
}
