package cz.uhk.pro.kappastar.repository;

import cz.uhk.pro.kappastar.Model.Movie;

public interface MovieRepositoryCustom {

    Movie updateMovie(Movie movie);
}
