package cz.uhk.pro.kappastar.repository;

import cz.uhk.pro.kappastar.Model.Reservation;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ReservationRepository extends MongoRepository<Reservation, String> {
}
