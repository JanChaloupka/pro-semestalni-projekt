package cz.uhk.pro.kappastar.repository;

import cz.uhk.pro.kappastar.Model.Reservation;

import java.util.List;

public interface ReservationRepositoryCustom {
    List<String> getReservedSeatsForScreening(String screeningId);

    List<Reservation> getReservationsByScreeningId(String screeeningId);
}
