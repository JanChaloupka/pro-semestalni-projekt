package cz.uhk.pro.kappastar.repository;

import cz.uhk.pro.kappastar.Model.Screening;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ScreeningRepository extends MongoRepository<Screening, String> {
}
