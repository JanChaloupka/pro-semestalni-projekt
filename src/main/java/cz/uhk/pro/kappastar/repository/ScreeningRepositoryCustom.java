package cz.uhk.pro.kappastar.repository;

import cz.uhk.pro.kappastar.Model.Screening;

import java.util.List;

public interface ScreeningRepositoryCustom {
    List<Screening> findScreeningByMovie(String movieId);

    List<Screening> getScreeningsInCinemaHall(String cinemaHallId);
}
