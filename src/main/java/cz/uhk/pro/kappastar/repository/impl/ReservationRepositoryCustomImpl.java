package cz.uhk.pro.kappastar.repository.impl;

import cz.uhk.pro.kappastar.Model.Reservation;
import cz.uhk.pro.kappastar.repository.ReservationRepositoryCustom;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class ReservationRepositoryCustomImpl implements ReservationRepositoryCustom {

    private final MongoTemplate mongoTemplate;

    public ReservationRepositoryCustomImpl(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    @Override
    public List<String> getReservedSeatsForScreening(String screeningId) {

        Query query = new Query();
        query.addCriteria(Criteria.where("screening.id").is(screeningId));
        List<Reservation> reservations = mongoTemplate.find(query, Reservation.class);

        List<String> reservedSeats = new ArrayList<>();


        reservations.forEach(reservation -> reservedSeats.addAll(reservation.getSeatsNumbers()));

        return reservedSeats;
    }

    @Override
    public List<Reservation> getReservationsByScreeningId(String screeningId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("screening.id").is(screeningId));
        List<Reservation> reservations = mongoTemplate.find(query, Reservation.class);
        return reservations;
    }
}
