package cz.uhk.pro.kappastar.repository.impl;

import cz.uhk.pro.kappastar.Model.dto.ScreeningDto;
import cz.uhk.pro.kappastar.Model.Movie;
import cz.uhk.pro.kappastar.Model.Screening;
import cz.uhk.pro.kappastar.Model.ScreeningTable;
import cz.uhk.pro.kappastar.repository.ScreeningRepositoryCustom;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.*;

@Repository
public class ScreeningRepositoryCustomImpl implements ScreeningRepositoryCustom {

    private final MongoTemplate mongoTemplate;

    public ScreeningRepositoryCustomImpl(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    public List<ScreeningTable> getScreeningTable(String date) {

        var dateFrom = LocalDateTime.parse(String.format("%sT00:00:00", date));
        var dateTo = LocalDateTime.parse(String.format("%sT23:59:59", date));

        List<ScreeningTable> screeningTableList = new ArrayList<>();

        var movies = mongoTemplate.findAll(Movie.class);

        if (movies.size() > 0) {
            movies.forEach(movie -> {

                Query query = new Query();
                query.addCriteria(Criteria.where("datetime").gte(dateFrom).lte(dateTo));
                query.addCriteria(Criteria.where("movie.id").is(movie.getId()));
                List<Screening> screenings = mongoTemplate.find(query, Screening.class);

                ArrayList<ScreeningDto>[] screeningsDto = new ArrayList[12];

                for (int i = 0; i < 12; i++) {
                    screeningsDto[i] = new ArrayList<>();
                }

                if (screenings.size() > 0) {

                    screenings.forEach(screening -> {

                        String time = new SimpleDateFormat("HH:mm").format(screening.getDatetime());
                        int hour = Integer.parseInt(time.split(":")[0]);

                        var screeningDto = new ScreeningDto(
                                screening.getId(),
                                screening.getMovie().getId(),
                                screening.getMovie().getName(),
                                screening.getCinemaHall().getId(),
                                screening.getCinemaHall().getName(),
                                screening.getDatetime(),
                                time,
                                screening.getTicketPrice(),
                                screening.isSupport3D(),
                                screening.isBetterSound()
                        );

                        int index = hour < 13 ? 0 : hour - 12;

                        screeningsDto[index].add(screeningDto);
                    });

                    screeningTableList.add(new ScreeningTable(movie, screeningsDto));
                }
            });
        }

        return screeningTableList;
    }

    @Override
    public List<Screening> findScreeningByMovie(String movieId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("movie.id").is(movieId));
        return mongoTemplate.find(query, Screening.class);
    }

    @Override
    public List<Screening> getScreeningsInCinemaHall(String cinemaHallId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("cinemaHall.id").is(cinemaHallId));
        return mongoTemplate.find(query, Screening.class);
    }

}
