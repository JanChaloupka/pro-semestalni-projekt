package cz.uhk.pro.kappastar.service;

import cz.uhk.pro.kappastar.Model.CinemaHall;

import java.util.List;
import java.util.Optional;

public interface CinemaHallService {

    CinemaHall addCinemaHall(CinemaHall cinemaHall);

    List<CinemaHall> getCinemaHalls();

    Optional<CinemaHall> getCinemaHall(String id);

    void deleteCinemaHall(String id);

    Optional<CinemaHall> getCinema(String id);
}
