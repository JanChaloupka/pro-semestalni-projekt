package cz.uhk.pro.kappastar.service;

import cz.uhk.pro.kappastar.Model.Movie;
import org.springframework.validation.BindingResult;

import java.util.List;
import java.util.Optional;

public interface MovieService {
    Optional<Movie> addMovie(Movie movie, BindingResult bindingResult);

    List<Movie> getAllMovies();

    void deleteMovie(String id);

    Optional<Movie> updateMovie(Movie movie, BindingResult bindingResult);

    Optional<Movie> getMovieById(String id);

}
