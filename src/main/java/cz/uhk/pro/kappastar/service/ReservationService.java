package cz.uhk.pro.kappastar.service;

import cz.uhk.pro.kappastar.Model.Reservation;
import cz.uhk.pro.kappastar.Model.dto.ReservationDto;
import org.springframework.validation.BindingResult;

import java.util.List;
import java.util.Optional;

public interface ReservationService {
    Optional<Reservation> addReservation(ReservationDto reservationDto, BindingResult bindingResult);

   void deleteReservation(String id);

   Optional<Reservation> getReservationById(String id);

   List<String> getReservedSeatsForScreening(String screeningId);

   List<Reservation> getReservationsByScreeningId(String screeningId);
}
