package cz.uhk.pro.kappastar.service;

import cz.uhk.pro.kappastar.Model.ScreeningDay;
import cz.uhk.pro.kappastar.Model.dto.ScreeningDto;
import cz.uhk.pro.kappastar.Model.Screening;
import cz.uhk.pro.kappastar.Model.ScreeningTable;
import org.springframework.validation.BindingResult;

import java.util.List;
import java.util.Optional;

public interface ScreeningService {

    List<Screening> getScreenings();

    Optional<Screening> getScreeningById(String id);

    Optional<Screening> addScreening(ScreeningDto screeningDto, BindingResult bindingResult);

    Optional<Screening> updateScreening(ScreeningDto screeningDto, BindingResult bindingResult);

    List<ScreeningTable> getScreeningTable(String date);

    List<ScreeningDay> getScreeningDays();

    void deleteScreening(String id);

    List<Screening> findScreeningByMovie(String id);
}
