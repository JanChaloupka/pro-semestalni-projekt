package cz.uhk.pro.kappastar.service.impl;

import cz.uhk.pro.kappastar.Model.CinemaHall;
import cz.uhk.pro.kappastar.repository.CinemaHallRepository;
import cz.uhk.pro.kappastar.service.CinemaHallService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@Service
public class CinemaHallServiceImpl implements CinemaHallService {

    private final CinemaHallRepository cinemaHallRepository;

    @Override
    public CinemaHall addCinemaHall(CinemaHall cinemaHall) {
        return cinemaHallRepository.save(cinemaHall);
    }

    @Override
    public Optional<CinemaHall> getCinemaHall(String id) {
        return cinemaHallRepository.findById(id);
    }

    @Override
    public List<CinemaHall> getCinemaHalls() {
        return cinemaHallRepository.findAll();
    }

    @Override
    public void deleteCinemaHall(String id) {
        cinemaHallRepository.deleteById(id);
    }

    @Override
    public Optional<CinemaHall> getCinema(String id) {
        return cinemaHallRepository.findById(id);
    }

}
