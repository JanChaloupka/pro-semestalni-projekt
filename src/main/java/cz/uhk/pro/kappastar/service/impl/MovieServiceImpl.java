package cz.uhk.pro.kappastar.service.impl;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import cz.uhk.pro.kappastar.Model.Movie;
import cz.uhk.pro.kappastar.repository.MovieRepository;
import cz.uhk.pro.kappastar.service.MovieService;
import cz.uhk.pro.kappastar.service.ScreeningService;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;

@Log4j2
@AllArgsConstructor
@Service
public class MovieServiceImpl implements MovieService {

    private final MovieRepository movieRepository;

    private final ScreeningService screeningService;

    @Override
    public Optional<Movie> getMovieById(String id) {
        return movieRepository.findById(id);
    }

    @Override
    public List<Movie> getAllMovies() {
        return movieRepository.findAll();
    }

    @Override
    public Optional<Movie> addMovie(Movie movie, BindingResult bindingResult) {

        validate(movie, bindingResult);

        if (!bindingResult.hasErrors()) {

            movie.setImage(getImageFromApi(movie.getName()));

            // Create new screening
            var created = movieRepository.save(movie);
            log.info(String.format("Screening with id: %s was created!", created.getId()));

            return Optional.of(created);
        }
        return Optional.empty();
    }

    @Override
    public Optional<Movie> updateMovie(Movie movie, BindingResult bindingResult) {

        if (movieRepository.findById(movie.getId()).isPresent()) {

            // Validate object
            validate(movie, bindingResult);

            if (!bindingResult.hasErrors()) {

                movie.setImage(getImageFromApi(movie.getName()));

                // Update existing movie
                var updated = movieRepository.save(movie);
                log.info(String.format("Movie with id: %s was updated!", updated.getId()));

                return Optional.of(updated);
            }
        }
        return Optional.empty();
    }

    @Override
    public void deleteMovie(String id) {
        if (movieRepository.findById(id).isPresent()) {

            AtomicBoolean canDeleteMovie = new AtomicBoolean(true);

            var screeningsWithMovie = screeningService.findScreeningByMovie(id);
            screeningsWithMovie.forEach(screening -> {
                if (screening.getDatetime().after(new Date())) {
                    canDeleteMovie.set(false);
                }
            });

            if (canDeleteMovie.get()) {

                // Delete screenings and reservations
                screeningsWithMovie.forEach(screening -> screeningService.deleteScreening(screening.getId()));

                // Delete movie
                movieRepository.deleteById(id);
                log.info(String.format("Movie with id: %s was deleted!", id));
            } else {
                log.error(String.format("Movie with id: %s cannot be deleted. There are planned screenings.", id));
            }
        }
    }

    private String getImageFromApi(String name) {

        String image = "/images/noimage.png";

        try {
            URL url = new URL("https://api.themoviedb.org/3/search/movie?api_key=20bf512e9f47bf6ddf1f88553f36929a&query=" + name.replace(" ", "+"));
            URLConnection request = url.openConnection();
            request.connect();

            JsonElement jsonElement = JsonParser.parseReader(new InputStreamReader((InputStream) request.getContent()));
            JsonObject jsonObject = jsonElement.getAsJsonObject();
            JsonObject result = jsonObject.get("results").getAsJsonArray().get(0).getAsJsonObject();
            image = String.format("http://image.tmdb.org/t/p/w500/%s", result.get("poster_path").getAsString());

        } catch (IndexOutOfBoundsException | IOException e) {
            log.warn(String.format("Plakát filmu '%s' nebyl nalezen.", name));
        }

        return image;
    }

    private void validate(Movie movie, BindingResult bindingResult) {

        // Check movie name
        if (movie.getName().isEmpty()) {
            bindingResult.addError(new FieldError("movie", "name", "Zadejte název filmu"));
            log.error(String.format("Invalid value of movie name, value: %s", movie.getName()));
        }

        // Check year
        if (movie.getYear() < 1900 || movie.getYear() > 2999) {
            bindingResult.addError(new FieldError("movie", "year", "Zadejte rok uvedení filmu"));
            log.error(String.format("Invalid value of movie year, value: %s", movie.getYear()));
        }

        // Check movie director
        if (movie.getDirector().isEmpty()) {
            bindingResult.addError(new FieldError("movie", "director", "Zadejte režiséra"));
            log.error(String.format("Invalid value of movie director, value: %s", movie.getDirector()));
        }

        // Check movie actors
        if (movie.getActors().isEmpty()) {
            bindingResult.addError(new FieldError("movie", "actors", "Zadejte herce"));
            log.error(String.format("Invalid value of movie actors, value: %s", movie.getActors()));
        }

        // Check movie description
        if (movie.getDescription().isEmpty()) {
            bindingResult.addError(new FieldError("movie", "description", "Zadejte popis"));
            log.error(String.format("Invalid value of movie description, value: %s", movie.getDescription()));
        }

        // Check movie length
        if (movie.getLength() < 1 || movie.getLength() > 3000) {
            bindingResult.addError(new FieldError("movie", "length", "Zadejte platnou délku filmu"));
            log.error(String.format("Invalid value of movie length, value: %s", movie.getLength()));
        }
    }
}
