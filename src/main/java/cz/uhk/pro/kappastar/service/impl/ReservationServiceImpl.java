package cz.uhk.pro.kappastar.service.impl;

import cz.uhk.pro.kappastar.Model.*;
import cz.uhk.pro.kappastar.Model.dto.ReservationDto;
import cz.uhk.pro.kappastar.repository.ReservationRepository;
import cz.uhk.pro.kappastar.repository.ScreeningRepository;
import cz.uhk.pro.kappastar.repository.impl.ReservationRepositoryCustomImpl;
import cz.uhk.pro.kappastar.service.ReservationService;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Log4j2
@AllArgsConstructor
@Service
public class ReservationServiceImpl implements ReservationService {

    private final ReservationRepository reservationRepository;

    private final ReservationRepositoryCustomImpl reservationRepositoryCustom;

    private final ScreeningRepository screeningRepository;

    @Override
    public Optional<Reservation> addReservation(ReservationDto reservationDto, BindingResult bindingResult) {

        Reservation reservation = new Reservation();

        // Validate object
        validate(reservationDto, reservation, bindingResult);

        if (!bindingResult.hasErrors()) {

            reservation.setPersonName(reservationDto.getPersonName());
            reservation.setEmail(reservationDto.getEmail());
            reservation.setSeatsNumbers(reservationDto.getSeatsNumbers());
            reservation.setDatetime(new Date());

            // Create new reservation
            var created = reservationRepository.save(reservation);
            log.info(String.format("Reservation with id: %s was created!", created.getId()));

            return Optional.of(reservation);
        }

        return Optional.empty();
    }

    @Override
    public Optional<Reservation> getReservationById(String id) {
        return reservationRepository.findById(id);
    }

    @Override
    public List<String> getReservedSeatsForScreening(String screeningId) {
        return reservationRepositoryCustom.getReservedSeatsForScreening(screeningId);
    }

    @Override
    public List<Reservation> getReservationsByScreeningId(String screeningId) {
        return reservationRepositoryCustom.getReservationsByScreeningId(screeningId);
    }

    @Override
    public void deleteReservation(String id) {
        if (reservationRepository.findById(id).isPresent()) {
            reservationRepository.deleteById(id);
            log.info(String.format("Reservation with id: %s was deleted!", id));
        }
    }

    private void validate(ReservationDto reservationDto, Reservation reservation, BindingResult bindingResult) {

        // Check person name
        if (reservationDto.getPersonName().isEmpty()) {
            bindingResult.addError(new FieldError("reservation", "personName", "Zadejte jméno a příjmení"));
            log.error("PersonName empty when creating reservation");
        }

        // Check valid email
        String regex = "^([\\w-\\.]+){1,64}@([\\w&&[^_]]+){2,255}.[a-z]{2,}$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(reservationDto.getEmail());
        if (!matcher.matches()) {
            bindingResult.addError(new FieldError("reservation", "email", "Neplatný email"));
            log.error(String.format("Invalid email address, value: %s", reservationDto.getEmail()));
        }

        // Check if screening exists
        var screening = screeningRepository.findById(reservationDto.getScreeningId());
        if (screening.isEmpty()) {
            bindingResult.addError(new FieldError("reservation", "screeningId", "Promítání nebylo nalezeno"));
            log.error(String.format("Screening with id: %s does not exists", reservationDto.getScreeningId()));
        } else {
            reservation.setScreening(screening.get());
        }

        // Check number of seats
        if (screening.isPresent() &&
                (reservationDto.getSeatsNumbers().size() == 0 ||
                        reservationDto.getSeatsNumbers().size() > screening.get().getCinemaHall().getSeatsCount())) {
            bindingResult.addError(new FieldError("reservation", "seatsNumbers", "Neplatný počet sedadel"));
            log.error(String.format("Invalid number of seats, value: %s", reservationDto.getSeatsNumbers().size()));
        }

        // Check if seats are reserved
        var reservedSeats = getReservedSeatsForScreening(reservationDto.getScreeningId());
        if (reservedSeats.size() > 0) {
            reservedSeats.forEach(seat -> {
                if (reservationDto.getSeatsNumbers().contains(seat)) {
                    bindingResult.addError(new FieldError("reservation", "seatsNumbers", String.format("Sedadlo číslo %s je již obsazeno", seat)));
                    log.error(String.format("Seat with number %s is already reserved", seat));

                    reservationDto.getSeatsNumbers().removeIf(s -> s.equals(seat));
                }
            });
        }
    }
}
