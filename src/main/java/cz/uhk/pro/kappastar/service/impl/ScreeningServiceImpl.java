package cz.uhk.pro.kappastar.service.impl;

import cz.uhk.pro.kappastar.Model.ScreeningDay;
import cz.uhk.pro.kappastar.Model.dto.ScreeningDto;
import cz.uhk.pro.kappastar.Model.Screening;
import cz.uhk.pro.kappastar.Model.ScreeningTable;
import cz.uhk.pro.kappastar.repository.MovieRepository;
import cz.uhk.pro.kappastar.repository.ScreeningRepository;
import cz.uhk.pro.kappastar.repository.impl.ScreeningRepositoryCustomImpl;
import cz.uhk.pro.kappastar.service.CinemaHallService;
import cz.uhk.pro.kappastar.service.ReservationService;
import cz.uhk.pro.kappastar.service.ScreeningService;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;

@Log4j2
@AllArgsConstructor
@Service
public class ScreeningServiceImpl implements ScreeningService {

    private final ScreeningRepository screeningRepository;
    private final ScreeningRepositoryCustomImpl screeningRepositoryCustom;

    private final CinemaHallService cinemaHallService;

    private final ReservationService reservationService;

    private final MovieRepository movieRepository;

    @Override
    public Optional<Screening> addScreening(ScreeningDto screeningDto, BindingResult bindingResult) {

        Screening screening = new Screening();

        // Validate object
        validate(screeningDto, screening, bindingResult);

        if (!bindingResult.hasErrors()) {
            screening.setBetterSound(screeningDto.isBetterSound());
            screening.setSupport3D(screeningDto.isSupport3D());
            screening.setDatetime(screeningDto.getDatetime());
            screening.setTicketPrice(screeningDto.getTicketPrice());

            // Create new screening
            var created = screeningRepository.save(screening);
            log.info(String.format("Screening with id: %s was created!", created.getId()));

            return Optional.of(created);
        }
        return Optional.empty();
    }

    @Override
    public Optional<Screening> updateScreening(ScreeningDto screeningDto, BindingResult bindingResult) {
        if (screeningRepository.findById(screeningDto.getId()).isPresent()) {

            Screening screening = new Screening();

            // Validate object
            validate(screeningDto, screening, bindingResult);

            if (!bindingResult.hasErrors()) {

                screening.setId(screeningDto.getId());
                screening.setBetterSound(screeningDto.isBetterSound());
                screening.setSupport3D(screeningDto.isSupport3D());
                screening.setDatetime(screeningDto.getDatetime());
                screening.setTicketPrice(screeningDto.getTicketPrice());

                // Update existing screening
                var updated = screeningRepository.save(screening);
                log.info(String.format("Screening with id: %s was updated!", updated.getId()));

                return Optional.of(updated);
            }
        }
        return Optional.empty();
    }

    @Override
    public List<ScreeningTable> getScreeningTable(String date) {
        return screeningRepositoryCustom.getScreeningTable(date);
    }

    @Override
    public List<ScreeningDay> getScreeningDays() {
        ArrayList<ScreeningDay> days = new ArrayList<>();

        Date today = new Date();

        String day1 = new SimpleDateFormat("yyyy-MM-dd").format(today);
        String day2 = new SimpleDateFormat("yyyy-MM-dd").format(new Date(today.getTime() + (1000 * 60 * 60 * 24)));
        String day3 = new SimpleDateFormat("yyyy-MM-dd").format(new Date(today.getTime() + (1000 * 60 * 60 * 24 * 2)));
        String day4 = new SimpleDateFormat("yyyy-MM-dd").format(new Date(today.getTime() + (1000 * 60 * 60 * 24 * 3)));
        String day5 = new SimpleDateFormat("yyyy-MM-dd").format(new Date(today.getTime() + (1000 * 60 * 60 * 24 * 4)));

        days.add(new ScreeningDay(day1, getDayName(day1)));
        days.add(new ScreeningDay(day2, getDayName(day2)));
        days.add(new ScreeningDay(day3, getDayName(day3)));
        days.add(new ScreeningDay(day4, getDayName(day4)));
        days.add(new ScreeningDay(day5, getDayName(day5)));

        return days;
    }

    @Override
    public void deleteScreening(String id) {
        if (screeningRepository.findById(id).isPresent()) {

            // Delete reservations related to current screening
            var reservations = reservationService.getReservationsByScreeningId(id);
            reservations.forEach(reservation -> reservationService.deleteReservation(reservation.getId()));
            log.info(String.format("Reservations for screening with id: %s were deleted!", id));

            // Delete screening
            screeningRepository.deleteById(id);
            log.info(String.format("Screening with id: %s was deleted!", id));
        }
    }

    @Override
    public List<Screening> findScreeningByMovie(String movieId) {
        return screeningRepositoryCustom.findScreeningByMovie(movieId);
    }

    @Override
    public List<Screening> getScreenings() {
        return screeningRepository.findAll(Sort.by("datetime").ascending());
    }

    @Override
    public Optional<Screening> getScreeningById(String id) {
        return screeningRepository.findById(id);
    }

    private String getDayName(String date) {

        DayOfWeek day = LocalDate.parse(date).getDayOfWeek();

        switch (day.getValue()) {
            case 1:
                return "Pondělí";
            case 2:
                return "Úterý";
            case 3:
                return "Středa";
            case 4:
                return "Čtvrtek";
            case 5:
                return "Pátek";
            case 6:
                return "Sobota";
            case 7:
                return "Neděle";
        }
        return "";
    }

    private void validate(ScreeningDto screeningDto, Screening screening, BindingResult bindingResult) {

        // Check if screening date is valid
        if (screeningDto.getDatetime() == null || screeningDto.getDatetime().before(new Date())) {
            bindingResult.addError(new FieldError("screening", "datetime", "Neplatné datum promítání"));
            log.error(String.format("Invalid value of screening datetime, value: %s", screeningDto.getDatetime()));
        }

        // Check if ticket price is valid
        if (screeningDto.getTicketPrice() < 0) {
            bindingResult.addError(new FieldError("screening", "ticketPrice", "Neplatná cena lístku"));
            log.error(String.format("Invalid value of ticket price, value: %s", screeningDto.getTicketPrice()));
        }

        // Check if cinemaHall exists
        var cinemaHall = cinemaHallService.getCinemaHall(screeningDto.getCinemaHallId());
        if (cinemaHall.isEmpty()) {
            bindingResult.addError(new FieldError("screening", "cinemaHallId", "Sál nebyl nalezen"));
            log.error(String.format("Cinema hall with id: %s does not exists", screeningDto.getCinemaHallId()));
        } else {
            screening.setCinemaHall(cinemaHall.get());
        }

        // Check if movie exists
        var movie = movieRepository.findById(screeningDto.getMovieId());
        if (movie.isEmpty()) {
            bindingResult.addError(new FieldError("screening", "movieId", "Film nebyl nalezen"));
            log.error(String.format("Movie with id: %s does not exists", screeningDto.getMovieId()));
        } else {
            screening.setMovie(movie.get());
        }

        // Check if cinemaHall supports 3D
        if (screeningDto.isBetterSound() && cinemaHall.isPresent()) {
            if (!cinemaHall.get().isBetterSound()) {
                bindingResult.addError(new FieldError("screening", "betterSound", "Vybraný sál nepodporuje zvuk DOLBY"));
                log.error(String.format("Cinema hall with id: %s does not support DOLBY", screeningDto.getCinemaHallId()));
            }
        }

        // Check if cinemaHall supports 3D
        if (screeningDto.isSupport3D() && cinemaHall.isPresent()) {
            if (!cinemaHall.get().isSupport3D()) {
                bindingResult.addError(new FieldError("screening", "support3D", "Vybraný sál nepodporuje 3D projekci"));
                log.error(String.format("Cinema hall with id: %s does not support 3D", screeningDto.getCinemaHallId()));
            }
        }

        // Check if screening time overlaps another screening
        if (cinemaHall.isPresent() && movie.isPresent() && screeningDto.getDatetime() != null) {

            var screeningsInCinemaHall = screeningRepositoryCustom.getScreeningsInCinemaHall(screeningDto.getCinemaHallId());

            AtomicBoolean screeningOverlaps = new AtomicBoolean(false);

            screeningsInCinemaHall.forEach(s -> {
                Date min = s.getDatetime();
                Date max = s.getEndDatetime();

                Date startTime = screeningDto.getDatetime();
                Date endTime = new Date(screeningDto.getDatetime().getTime() + ((long) movie.get().getLength() * 60 * 1000));

                if ((startTime.after(min) && startTime.before(max)) ||
                        endTime.after(min) && endTime.before(max)) {
                    screeningOverlaps.set(true);
                }
            });

            if (screeningOverlaps.get()) {
                bindingResult.addError(new FieldError("screening", "datetime", "V tento čas se již v sále promítá jiný film"));
                log.error("There is another screening planned in that time");
            }

        }
    }
}
