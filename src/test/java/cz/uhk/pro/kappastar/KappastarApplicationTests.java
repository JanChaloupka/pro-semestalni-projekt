package cz.uhk.pro.kappastar;

import cz.uhk.pro.kappastar.Model.CinemaHall;
import cz.uhk.pro.kappastar.Model.Movie;
import cz.uhk.pro.kappastar.Model.Reservation;
import cz.uhk.pro.kappastar.Model.Screening;
import cz.uhk.pro.kappastar.Model.dto.ReservationDto;
import cz.uhk.pro.kappastar.Model.dto.ScreeningDto;
import cz.uhk.pro.kappastar.service.CinemaHallService;
import cz.uhk.pro.kappastar.service.MovieService;
import cz.uhk.pro.kappastar.service.ReservationService;
import cz.uhk.pro.kappastar.service.ScreeningService;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.validation.BindingResult;

import java.sql.Time;
import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;


@Disabled
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class KappastarApplicationTests {

    @Autowired
    CinemaHallService cinemaHallService;

    @Autowired
    MovieService movieService;

    @Autowired
    ScreeningService screeningService;

    @Autowired
    ReservationService reservationService;


    private final BindingResult bindingResult = mock(BindingResult.class);

    String[][] seats = {
            {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"},
            {"11", "12", "13", "14", "15", "16", "17", "18", "19", "20"},
            {"21", "22", "23", "24", "25", "26", "27", "28", "29", "30"},
            {"31", "32", "33", "34", "35", "36", "37", "38", "39", "40"},
            {"41", "42", "43", "44", "45", "46", "47", "48", "49", "50"},
            {"51", "52", "53", "54", "55", "56", "57", "58", "59", "60"},
    };

    CinemaHall cinemaHall = new CinemaHall("CinemaHall1", "Sál 1", false, true, seats);


    // List of ids to be deleted after tests are finished
    private final List<String> createdIds = new ArrayList<>();


    @Test
    void cinemaHallCheck() {
        var created = cinemaHallService.addCinemaHall(cinemaHall);
        var retrieved = cinemaHallService.getCinemaHall(created.getId());
        addCreatedId(created.getId());

        assertThat(retrieved).isPresent();
        assertThat(created).isEqualTo(retrieved.get());
    }

    @Test
    void movieCheck() {

        Movie movie = new Movie("No time to Die", 2021, "Cary Fukunaga", "Daniel Creig", "Movie about agent 007", 210, "");

        // Check created movie
        var created = movieService.addMovie(movie, bindingResult);
        assertThat(created).isPresent();
        addCreatedId(created.get().getId());

        // Check retrieved movie by id
        var retrieved = movieService.getMovieById(created.get().getId());
        assertThat(retrieved).isPresent();

        // Check that created and updated are equal
        assertThat(retrieved).isEqualTo(created);

        // Check updated movie
        Movie updatedMovie = new Movie(movie.getId(), "No time to Die", 2021, "Cary Fukunaga", "Daniel Creig", "Movie about agent 007", 220, "");
        var updated = movieService.updateMovie(updatedMovie, bindingResult);
        assertThat(updated).isPresent();
        assertThat(updated.get().getLength()).isEqualTo(220);

    }

    @Test
    void testScreeningsWithReservation() {
        cinemaHallService.addCinemaHall(cinemaHall);

        Movie movie = new Movie("Movie1", 1999, "Jason Statham", "Daniel K, Jan Ch", "General movie", 60, "");
        var createdMovie = movieService.addMovie(movie, bindingResult);
        createdIds.add(createdMovie.get().getId());

        ScreeningDto screeningDto = new ScreeningDto(createdMovie.get().getId(), createdMovie.get().getName(), cinemaHall.getId(), cinemaHall.getName(), new Date(), new Time(13, 39, 0).toString(), 40, false, true);

        // Create screenig
        var screening = screeningService.addScreening(screeningDto, bindingResult);
        createdIds.add(screening.get().getId());
        addCreatedId(screening.get().getId());

        // Check retrieved screening by id
        var retrieved = screeningService.getScreeningById(screening.get().getId());
        assertThat(retrieved).isEqualTo(screening);

        List<String> seatsForReservation = Arrays.asList("1","2");

        // Create reservation
        ReservationDto reservationDto = new ReservationDto("Reservation1", screening.get().getId(), "Jan", "somemail@email.com", new Date(), seatsForReservation);
        var createdReservation = reservationService.addReservation(reservationDto, bindingResult);
        addCreatedId(createdReservation.get().getId());

        //Check reservaton
        assertThat(reservationDto.getEmail()).isEqualTo(createdReservation.get().getEmail());
        assertThat(reservationDto.getPersonName()).isEqualTo(createdReservation.get().getPersonName());
        assertThat(reservationDto.getSeatsNumbers()).isEqualTo(createdReservation.get().getSeatsNumbers());
    }

    @AfterAll
    void cleanup() {
        createdIds.forEach(id -> {
            screeningService.deleteScreening(id);
            cinemaHallService.deleteCinemaHall(id);
            movieService.deleteMovie(id);
            reservationService.deleteReservation(id);
        });
        cinemaHallService.deleteCinemaHall(cinemaHall.getId());
    }

    private void addCreatedId(String id) {
        createdIds.add(id);
    }
}
